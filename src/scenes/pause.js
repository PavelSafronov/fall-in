import Phase from "../lib/phaser.js";

export default class Pause extends Phaser.Scene {
  /** @type {string} */
  sceneToWake;

  constructor() {
    super("pause");
  }

  create() {
    const width = this.scale.width;
    const height = this.scale.height;

    this.add
      .text(width * 0.5, height * 0.5, "Paused", {
        fontSize: 48,
      })
      .setOrigin(0.5);

    this.input.keyboard.once("keydown-P", () => {
      this.scene.wake(this.sceneToWake);
      this.scene.stop();
    });
  }

  init(sceneToWake) {
    this.sceneToWake = sceneToWake;
  }
}
